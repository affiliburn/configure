#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `configure` package."""

import pytest


from configure import get_config


# def get_dict_paths(nested_dict, current_parts=None):
#     if current_parts is None:
#         current_parts = []
#     else:
#         current_parts = list(current_parts)
#         current_parts = copy(current_parts)
#     for key, value in nested_dict.items():
#         current_parts.append(key)
#         if isinstance(value, dict):
#             if len(value.keys()) == 0:
#                 yield tuple(copy(current_parts))
#                 current_parts = current_parts[:-1]
#             else:
#                 for path in get_dict_paths(value, current_parts):
#                     yield path
#         else:
#             yield tuple(copy(current_parts))
#             current_parts = current_parts[:-1]
#
#
# test_dict = {'test': {},
#  't2': {'t3': {},
#         't4': {},
#         't5': {'t6': {},
#                't7': {'t8': 'default_value'}}}
#  }
# result = {('test',), ('t2', 't3'), ('t2', 't4'), ('t2', 't5', 't6'), ('t2', 't5', 't7', 't8')}
#
# assert set(get_dict_paths(test_dict)) == result


yaml_content = """
yaml_nested_1:
  yaml_value_1: 1
  yaml_nested_2:
    yaml_value_2: 2
    yaml_value_3: 3
yaml_value_4: 4"""

dictionary_1 = {'yaml_value_4': 5, 'dict_value_1': 1}

expected_config_1 = {
    'yaml_nested_1': {
        'yaml_value_1': 1,
        'yaml_nested_2': {
            'yaml_value_2': 2,
            'yaml_value_3': 3
        }
    },
    'yaml_value_4': 4
}

expected_config_2 = {
    'yaml_nested_1': {
        'yaml_value_1': 1,
        'yaml_nested_2': {
            'yaml_value_2': 2,
            'yaml_value_3': 3
        }
    },
    'yaml_value_4': 5,
    'dict_value_1': 1
}


def test_get_config(tmpdir):
    global yaml_content, expected_config_1, expected_config_2
    default_conf = tmpdir.mkdir("base").join("default.yaml")
    default_conf.write(yaml_content)
    config_1 = get_config(dictionary_override=None, file_path=default_conf.strpath,
                          environment_variables=False, command_line=False, file_as_template=False, refresh_config=True)
    assert config_1 == expected_config_1
    config_2 = get_config(dictionary_override=dictionary_1, file_path=default_conf.strpath,
                          environment_variables=False, command_line=False, file_as_template=False, refresh_config=False)
    assert config_2 == expected_config_1
    config_3 = get_config(dictionary_override=dictionary_1, file_path=default_conf.strpath,
                          environment_variables=False, command_line=False, file_as_template=False, refresh_config=True)
    assert config_3 == expected_config_2
