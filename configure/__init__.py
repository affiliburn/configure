# -*- coding: utf-8 -*-

"""
Configure: Because we need a configuration standard.
"""

__author__ = """Itay Elbirt"""
__email__ = 'anotahacou@gmail.com'
__version__ = '0.6.2'


import os
import logging
from copy import deepcopy
from argparse import ArgumentParser


# Generic


class ConfigError(Exception):
    pass


class ConfigSource:
    def __init__(self, optional=False):
        self.optional = optional

    def get_config(self, current_config=None):
        current_config = deepcopy(current_config)
        if current_config is None:
            current_config = {}

        try:
            new_config = self._get_config(current_config)
        except:
            if self.optional:
                new_config = current_config
            else:
                raise
        current_config.update(new_config)
        return current_config


# File sources


class YamlFile(ConfigSource):
    def __init__(self, file_path, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.file_path = file_path

    def _get_config(self, current_config=None):
        import yaml
        new_config = yaml.load(open(self.file_path))
        return new_config


class File(ConfigSource):
    def __init__(self, file_path, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.file_path = file_path
        if file_path.endswith('.yaml') or file_path.endswith('.yml'):
            self.file_source = YamlFile(file_path)
        else:
            raise NotImplementedError("Only support .yaml files")

    def _get_config(self, current_config=None):
        new_config = self.file_source.get_config(current_config=current_config)
        return new_config


# Environment sources


class AllEnvironmentVariables(ConfigSource):
    """In this config source, __ means a sub division of a config"""
    def _get_config(self, current_config=None):
        new_config = {}
        for key, value in os.environ.items():
            key = key.lower().replace('-', '_')
            if '__' in key:
                current = new_config
                keys = key.split('__')
                for subkey in keys[:-1]:
                    current = current[subkey]
                current[keys[-1]] = value
            else:
                new_config[key] = value
        return new_config


class Arguments(ArgumentParser):
    """In this config source, __ or -- means a sub division of a config"""
    def parse_args(self, args=None, namespace=None):
        args, argv = self.parse_known_args(args, namespace)
        if argv:
            msg = 'unrecognized arguments: {}'.format(argv)
            logging.warning(msg)
        return args

    def get_config(self, current_config=None):
        current_config = deepcopy(current_config)
        if current_config is None:
            current_config = {}

        new_config = self._get_config(current_config)
        current_config.update(new_config)
        return current_config

    def _get_config(self, current_config=None):
        new_config = {}
        for key, value in vars(self.parse_args()).items():
            key = key.lower().replace('-', '_')
            if '__' in key:
                current = new_config
                keys = key.split('__')
                for subkey in keys[:-1]:
                    current = current[subkey]
                current[keys[-1]] = value
            else:
                new_config[key] = value
        return new_config


# Additional sources


class HardCoded(ConfigSource):
    def __init__(self, dictlike, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.source = dictlike

    def _get_config(self, current_config=None):
        return deepcopy(self.source)


class Configuration(ConfigSource):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.sources = []

    def add_source(self, config_source):
        self.sources.append(config_source)

    def get_config(self, current_config=None):
        if current_config is None:
            current_config = {}
        for source in self.sources:
            if isinstance(source, ConfigSource) or isinstance(source, Arguments):
                new_config = source.get_config(current_config)
                current_config.update(new_config)
            else:
                # Assume dictionary
                current_config.update(source)
        return current_config


def iterate_keys(dictionary, current_keys=None):
    if current_keys is None:
        current_keys = tuple()
    for key, value in dictionary.items():
        if not isinstance(value, dict):
            yield (*current_keys, key), value
        else:
            for keys, value in iterate_keys(dictionary[key], (*current_keys, key)):
                yield keys, value


class Filtered(ConfigSource):
    """Filtered configuration source can only override past configurations"""
    def __init__(self, config_source, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.config_source = config_source

    def _get_config(self, current_config=None):
        unfiltered_config = self.config_source.get_config(current_config=current_config)
        result_config = {}
        for keys, value in iterate_keys(current_config):
            current_unfiltered = unfiltered_config
            current_result = result_config
            for key in keys[:-1]:
                current_unfiltered = current_unfiltered[key]
                current_result = current_result.setdefault(key, {})
            last_key = keys[-1]
            current_result[last_key] = current_unfiltered[last_key]
        return result_config


default_arguments = Arguments()


def setup_command_line(*args, **kwargs):
    # TODO: Write test
    global default_arguments
    default_arguments = Arguments(*args, **kwargs)
    return default_arguments


stored_config = None


def get_config(dictionary_override=None, file_path='config.yaml', environment_variables=True, command_line=True,
               file_as_template=True, refresh_config=False):
    # TODO: Write test for command line on/off
    global stored_config
    if stored_config is not None and not refresh_config:
        return deepcopy(stored_config)

    stored_config = {}

    config = Configuration()
    if file_path:
        config.add_source(File(file_path, optional=True))
    if environment_variables:
        if file_as_template and file_path is not None:
            Filtered(AllEnvironmentVariables())
        else:
            config.add_source(AllEnvironmentVariables())
    if command_line:
        if file_as_template and file_path is not None:
            Filtered(default_arguments)
        else:
            config.add_source(default_arguments)
    if dictionary_override:
        config.add_source(dictionary_override)
    stored_config = config.get_config()
    return deepcopy(stored_config)


def get_stored_config():
    global stored_config
    if stored_config is None:
        return get_config_no_store()
    return deepcopy(stored_config)


def get_config_no_store():
    '''Get configuration by basic methods but don't store it.'''
    global stored_config
    stored_config_backup = stored_config
    config = get_config()
    stored_config = stored_config_backup
    return config
