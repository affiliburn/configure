=========
configure
=========


.. image:: https://img.shields.io/pypi/v/configure.svg
        :target: https://pypi.python.org/pypi/configure

.. image:: https://img.shields.io/travis/Tapuzi/configure.svg
        :target: https://travis-ci.org/Tapuzi/configure

.. image:: https://readthedocs.org/projects/configure/badge/?version=latest
        :target: https://configure.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




Configuration library for the greater good.


* Free software: MIT license
* Documentation: https://configure.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
